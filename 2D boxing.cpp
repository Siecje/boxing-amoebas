// Cody Scott
// 0487276
// Dr. Benson
// October 30, 2012
// Boxing Amoeba Game

#include <stdlib.h>
#ifdef __APPLE__
	#include <GLUT/glut.h>
#else
	#include <GL/glut.h>
#endif

#include <stdio.h>
#include <math.h>
#include <time.h>

#define PI 3.14159

//Controls
int p1Up = 'w';
int p1Down = 's';
int p1Right = 'd';
int p1Left = 'a';
int p1Attack = '2';
int p1Defend = '1';

int p2Up = 'i';
int p2Down = 'k';
int p2Right = 'l';
int p2Left = 'j';
int p2Attack = '9';
int p2Defend = '0';

//player colours
float p1Colour[] = {0.0f, 1.0f, 1.0f};
float p2Colour[] = {1.0f, 0.0f, 1.0f};

//arm colours
float attackColour[] = {0.75f, 0.45f, 0.87f};
float defendColour [] ={0.25f, 0.25f, 0.25f};

//Window Size
int width = 800;
int height = 800;

//Screen Edges
float screenTop = 100;
float screenBottom = 0;
float screenRight = 100;
float screenLeft = 0;

float p1XStarting = screenRight/2;
float p1YStarting =  75.0;

float p2XStarting = screenRight/2;
float p2YStarting = 25.0;

//Player1's Centre Location
float p1X = p1XStarting;
float p1Y = p1YStarting;

//Player2's Centre Location
float p2X = p2XStarting;
float p2Y = p2YStarting;

float startingRadius = 8.0;
float maxRadius = 20.0;

//initial Radius of the players
float p1Radius = startingRadius;
float p2Radius = startingRadius;

//initial angle 0 radians
float p1Angle = PI;
float p2Angle = 0.0;

float bounceFactor = 200.0;

//player attributes
GLint playerSlices = 200;
GLint playerStacks = 200;

//Set dynamically by the clock
float moveSpeed;
float turnSpeed;

bool playersCollided = false;

float poison1X = 5;
float poison1Y = 5;

float poison2X = 95;
float poison2Y = 95;

float poisonRadius = 2;

float poisonColour [] = {0.3, 1.0, 0.0};

//Used to check for key presses
bool *keyStates = new bool [256];

float attackDelay = 5;
float p1LastAttack = 999;
float p2LastAttack = 999;

//Used to control framerate
clock_t deltaTime = 0.0;
clock_t currentTime = 0.0;
clock_t lastTime = clock();

void changeSize(int width, int height){
	// Prevent a divide by zero, when window is too short
	// (you cant make a window of zero width).
	if (height == 0){
		height = 1;
	}
    // Use the Projection Matrix
	glMatrixMode(GL_PROJECTION);

    // Reset Matrix to identity matrix
	glLoadIdentity();

	// Set the viewport to be the entire window
	glViewport(0, 0, (GLsizei) width, (GLsizei) height);
	
	glOrtho(0, 100, 0, 100,  -100.0f, 100.0f);

	// Get Back to the Modelview
	glMatrixMode(GL_MODELVIEW);
}

void attack(){
	glColor3fv(attackColour);

	//Hard coded around the origin so it rotates with the player
	glutSolidSphere(5, 20, 20);
	
	glTranslatef(2, 2, 0);
	glutSolidSphere(5, 20, 20);
	glTranslatef(-(2), -(2), 0);
	
	glTranslatef(8, 8, 0);
	glutSolidSphere(5, 20, 20);
	glTranslatef(-(8), -(8), 0);

	glTranslatef(11, 11, 0);
	glutSolidSphere(5, 20, 20);
	glTranslatef(-(11), -(11), 0);

	glTranslatef(11, 13, 0);
	glutSolidSphere(5, 20, 20);
	glTranslatef(-(11), -(13), 0);

	glTranslatef(10, 15, 0);
	glutSolidSphere(5, 20, 20);
	glTranslatef(-(10), -(15), 0);

	glTranslatef(9, 17, 0);
	glutSolidSphere(5, 20, 20);
	glTranslatef(-(9), -(17), 0);

	glColor3f(1.0, 0.0, 0.0);

	glTranslatef(8, 20, 0);
	glutSolidSphere(5, 20, 20);
	glTranslatef(-(8), -(20), 0);
}

void defend(){
	glColor3fv(defendColour);

	//Hard coded around the origin so it rotates with the player
	glutSolidSphere(5, 20, 20);
	
	glTranslatef(-2, 2, 0);
	glutSolidSphere(5, 20, 20);
	glTranslatef(2, -(2), 0);

	glTranslatef(-8, 8, 0);
	glutSolidSphere(5, 20, 20);
	glTranslatef(8, -8, 0);

	glTranslatef(-11, 11, 0);
	glutSolidSphere(5, 20, 20);
	glTranslatef(11, -11, 0);

	glTranslatef(-11, 13, 0);
	glutSolidSphere(5, 20, 20);
	glTranslatef(11, -13, 0);

	glTranslatef(-5, 15, 0);
	glutSolidSphere(5, 20, 20);
	glTranslatef(5, -(15), 0);

	glTranslatef(-3, 16, 0);
	glutSolidSphere(5, 20, 20);
	glTranslatef(3, -(16), 0);

	glColor3f(1.0, 0.0, 0.0);

	glTranslatef(0, 15, 0);
	glutSolidSphere(5, 20, 20);
	glTranslatef(0, -(15), 0);
}

bool collision(float circle1X, float circle1Y, float radius1, float circle2X, float circle2Y, float radius2){
	//checks if player1 has collided with player2
	if ( sqrt( ( circle2X - circle1X ) * ( circle2X-circle1X )  + ( circle2Y-circle1Y ) * ( circle2Y-circle1Y ) ) < ( radius1 + radius2 ) ) {
		return true;
	}
	return false;
}

bool havePlayerCollided(){
	if (collision(p1X, p1Y, p1Radius, p2X, p2Y, p2Radius)){
		return true;
	}
	return false;
}

void resetPlayers(){
	poison1X = 5;
	poison1Y = 5;
	poison2X = 95;
	poison2Y = 95;
	p1Angle = PI;
	p2Angle = 0.0;
	p1Radius = startingRadius;
	p2Radius = startingRadius;
	p1X = p1XStarting;
	p1Y = p1YStarting;
	p2X = p2XStarting;
	p2Y = p2YStarting;
}

void checkAttacksCollision(){
	//checks if player1's last circle of the attacking arm has hit player2
	if (keyStates[p1Attack]){
        if(collision (sin(-p1Angle+0.3752458f) * 21.54066f +  p1X, cos(-p1Angle+0.3752458f) * 21.54066f + p1Y, 5, p2X, p2Y, p2Radius)){
			if (p1Radius < maxRadius){
				p1Radius += 1.0;
			}
            p2Radius -= 2.0;
			if (p2Radius < 2){
				resetPlayers();
			}
        }
    }
	//checks if player2's last circle of the attacking arm has hit player1
	if (keyStates[p2Attack]){
		if(collision (sin(-p2Angle+0.3752458f) * 21.54066f +  p2X, cos(-p2Angle+0.3752458f) * 21.54066f + p2Y, 5, p1X, p1Y, p1Radius)){
			if (p2Radius < maxRadius){
				p2Radius += 1.0;
			}
			p1Radius -= 2.0;
			if (p1Radius < 2){
				resetPlayers();
			}
		}
	}
}

void checkDefendsCollision(){
	//checks if player1's last circle of the attacking arm has hit player2
	if (keyStates[p1Defend]){
		if(collision (sin(-p1Angle) * 15.0f +  p1X, cos(-p1Angle) * 15.0f + p1Y, 5, p2X, p2Y, p2Radius)){
			if (p1Angle >= 0 && p1Angle <= PI/2){
				p2Y += cos(p1Angle) * moveSpeed;
				p2X -= sin(p1Angle) * moveSpeed;
			}
			else if (p1Angle > PI/2 && p1Angle <= PI){
				p2Y += cos(p1Angle) * moveSpeed;
				p2X -= sin(p1Angle) * moveSpeed;
			}
			else if (p1Angle > PI && p1Angle <= (3/2)*PI){
				p2Y -= cos(p1Angle) * moveSpeed;
				p2X -= sin(p1Angle) * moveSpeed;
			}
			else if (p1Angle > (3/2)*PI && p1Angle <= 2*PI){
				p2Y += cos(p1Angle) * moveSpeed;
				p2X -= sin(p1Angle) * moveSpeed;
			}
        }
    }
	//checks if player2's last circle of the attacking arm has hit player1
	if (keyStates[p2Defend]){
		if(collision (sin(-p2Angle) * 15.0f +  p2X, cos(-p2Angle) * 15.0f + p2Y, 5, p1X, p1Y, p1Radius)){
			if (p2Angle >= 0 && p2Angle <= PI/2){
				p1Y += cos(p2Angle) * moveSpeed;
				p1X -= sin(p2Angle) * moveSpeed;
			}
			else if (p2Angle > PI/2 && p2Angle <= PI){
				p1Y += cos(p2Angle) * moveSpeed;
				p1X -= sin(p2Angle) * moveSpeed;
			} 
			else if (p2Angle > PI && p2Angle <= (3/2)*PI){
				p1Y -= cos(p2Angle) * moveSpeed;
				p1X -= sin(p2Angle) * moveSpeed;
			}
			else if (p2Angle > (3/2)*PI && p2Angle <= 2*PI){
				p1Y += cos(p2Angle) * moveSpeed;
				p1X -= sin(p2Angle) * moveSpeed;
			}
        }
	}
}

void positionPlayer(float *x, float *y, float *angle, float *radius, int up, int down, int right, int left, bool collided){
	//check for player bounce
	if (*y + *radius > screenTop){
		*y = screenTop - *radius;
		*angle = PI - *angle;
	}
    else if (*y - *radius < screenBottom){
		*y = screenBottom + *radius;
		*angle = PI - *angle;
	}
	if (*x + *radius > screenRight){
 		*x = screenRight - *radius;
		*angle = 2*PI - *angle;
    }
	else if (*x - *radius < screenLeft){
		*x = screenLeft + *radius;
		*angle = 2*PI - *angle;
	}

	//Move Player
	if (!collided && keyStates[up]){		
		if (*angle >= 0 && *angle <= PI/2){
			*y += cos(*angle) * moveSpeed;
			*x -= sin(*angle) * moveSpeed;
		} 
		else if (*angle > PI/2 && *angle <= PI){
			*y += cos(*angle) * moveSpeed;
			*x -= sin(*angle) * moveSpeed;
		} 
		else if (*angle > PI && *angle <= (3/2)*PI){ 
			*y -= cos(*angle) * moveSpeed; 
			*x -= sin(*angle) * moveSpeed; 
		} 
		else if (*angle > (3/2)*PI && *angle <= 2*PI){ 
			*y += cos(*angle) * moveSpeed; 
			*x -= sin(*angle) * moveSpeed; 
		}
	}

	if (!collided && keyStates[down]){
		if (*angle >= 0 && *angle <= PI/2){
			*y -= cos(*angle) * moveSpeed;
			*x += sin(*angle) * moveSpeed;
		} 
		else if (*angle > PI/2 && *angle <= PI){
			*y -= cos(*angle) * moveSpeed;
			*x += sin(*angle) * moveSpeed;
		} 
		else if (*angle > PI && *angle <= (3/2)*PI){ 
			*y += cos(*angle) * moveSpeed;
			*x += sin(*angle) * moveSpeed;
		} 
		else if (*angle > (3/2)*PI && *angle <= 2*PI){ 
			*y -= cos(*angle) * moveSpeed;
			*x += sin(*angle) * moveSpeed;
		}
	}
	if (keyStates[right]){
		*angle += turnSpeed;
	}
	if (keyStates[left]){
		*angle -= turnSpeed;
	}
}

void processNormalKeys(unsigned char key, int x, int y) {
	//ESC
	if (key == 27){
		exit(0);
	}
	if (key == p1Attack || key == p2Attack){
		if(key == p1Attack && p1LastAttack > attackDelay){
			p1LastAttack = 0;
			keyStates[key] = true;
		}
		else if (key == p2Attack && p2LastAttack > attackDelay){
			p2LastAttack = 0;
			keyStates[key] = true;
		}
	}
	else{
		keyStates[key] = true;
	}
}

void keyUp (unsigned char key, int x, int y){
	keyStates[key] = false; // Set the state of the current key to not pressed
}

void resetAngle(float *angle){
	if (*angle >= 2*PI){
		*angle -= 2*PI;
	}
	else if (*angle <= 0){
		*angle += 2*PI;
	}
}

void checkPoisonCollision(){
	//if player1 gets hit
	if (collision(poison1X, poison1Y, poisonRadius, p1X, p1Y, p1Radius) || collision(poison2X, poison2Y, poisonRadius, p1X, p1Y, p1Radius)){
		resetPlayers();
	}
	//if player2 gets hit
	else if (collision(poison1X, poison1Y, poisonRadius, p2X, p2Y, p2Radius) || collision(poison2X, poison2Y, poisonRadius, p2X, p2Y, p2Radius)){
		resetPlayers();
	}
}

void managePlayer(float x, float y, float angle, float radius, int defendKey, int attackKey, float *colour){
	// Transformations for Player1
	glTranslatef(x, y, 0);
	glRotatef(angle * (180/PI), 0, 0, 1);
	//BEGIN TRANSFORMATIONS
		//placed inside the transformations to be applied in the rotations
		if (keyStates[defendKey]){
			defend();
		}
		else if (keyStates[attackKey]){
			attack();
		}

		// Set Colour of Player1
		glColor3fv(colour);

		//draw player after arms for Painter's algorithmn
		glutSolidSphere(radius, playerSlices, playerStacks);
		
		//draw small white circle for direction
		glColor3f(1, 1, 1);
		glTranslatef(0, radius*0.8, 0);
		glutSolidSphere(radius/4, 20,20);
		glTranslatef(0, -(radius*0.8), 0);
	//END TRANSFORMATIONS
	glRotatef(-(angle * (180/PI)), 0, 0, 1);
	glTranslatef(-x, -y,0);
}

void managePoison(float *x, float *y	, float *colour, float radius, float lower, float upper){
	if (*x > 100){
		*x = 95;
	}
	else if (*x < 0){
		*x = 5;
	}
	if (*y > 100){
		*y = 95;
	}	
	else if (*y < 0){
		*y = 5;
	}

	*x += lower + (float)rand()/((float)RAND_MAX/(upper-(lower)));
	*y += lower + (float)rand()/((float)RAND_MAX/(upper-(lower)));
	//move poison objects
	glTranslatef(*x, *y, 0);

		glColor3fv(colour);

		glutSolidSphere(radius, playerSlices, playerStacks);

	glTranslatef(-*x, -*y, 0);
}

void renderScene(){
	//** CONTROL FRAME RATE START **
	currentTime = clock();
	deltaTime = currentTime - lastTime;
	
	if (deltaTime < 1){
		return;
	}

	moveSpeed = deltaTime * 0.02;
	turnSpeed = deltaTime * 0.005;

	lastTime = currentTime;
	// ** CONTROL FRAME RATE END **

	p1LastAttack += 0.03;
	p2LastAttack += 0.03;

	resetAngle(&p1Angle);
	resetAngle(&p2Angle);

	checkAttacksCollision();
	checkDefendsCollision();
	checkPoisonCollision();

	// Clear Color and Depth Buffers
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//manages translations, roations, attack and defense animations
	managePlayer(p1X, p1Y, p1Angle, p1Radius, p1Defend, p1Attack, p1Colour);
	managePlayer(p2X, p2Y, p2Angle, p2Radius, p2Defend, p2Attack, p2Colour);

	managePoison(&poison1X, &poison1Y, poisonColour, poisonRadius, -2, 2);
	managePoison(&poison2X, &poison2Y, poisonColour, poisonRadius, -2, 2);

	playersCollided = havePlayerCollided();

	positionPlayer(&p1X, &p1Y, &p1Angle, &p1Radius, p1Up, p1Down, p1Right, p1Left, playersCollided);
	positionPlayer(&p2X, &p2Y, &p2Angle, &p2Radius, p2Up, p2Down, p2Right, p2Left, playersCollided);

	glFlush();
	glutSwapBuffers();
}

void myInit(){
	//Enable random seed
	srand((unsigned)time(0));
	//Initilize all keys and Special keys to false
	for ( int i = 0;  i < 256; i++){
		keyStates[i] = false;
	}
}

int main(int argc, char **argv){
	// init GLUT and create window
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(width, height);
	glutCreateWindow("Boxing Game");

	myInit();

	// register callbacks
	glutDisplayFunc(renderScene);
	glutReshapeFunc(changeSize);
	glutIdleFunc(renderScene);

	glutKeyboardFunc(processNormalKeys);
	glutKeyboardUpFunc(keyUp);

	glutMainLoop();

	return 0;
}